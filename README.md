# Jfinal 扩展插件集合

#### 项目介绍
针对自己在实际项目中需要使用到的功能做一个插件合辑
#### 软件架构
基于 JFinal 3.5


#### 安装教程



#### 使用说明

### 1. Log4j2Plugin

描述
```
# 覆盖了 *com.jfinal.log.Log* 抽象类，新增了一下方法，之前有联系过 詹波大大 ，他说后续版本会新增这个支持
public abstract void debug(String message,Object... params);
public abstract void info(String message,Object... params);
public abstract void error(String message,Object... params);
```
使用方式
```
# jfinal web 项目调用
public void configConstant(Constants constants) {
    /**
     * 设置日志工厂为log4j2
    */
    constants.setLogFactory(new Log4j2Plugin().getLogFactory());
}

# 可独立于WEB项目之外的调用，可传参告知配置文件地址，文件默认在${classpath}/config/log4j2.xml
new Log4j2Plugin().start();
```

### 2. MysqlDataSourcePlugin

主要针对我自己框架所开发的插件，主要支持如下
```
1. 多数据源支持，配置文件采用 ini 的格式进行编写
2. 支持 JFinal Model 的@Model 注解扫描映射
3. 支持 SQL 模板化（类似 MyBatis的Mapper文件）,扫描指定路径下的 *.stl 文件，文件里面格式是用 enjoy template来解析
```

使用方式（适用于web，非web可以尝试，我未进行测试）
```
    public void configPlugin(Plugins plugins) {
        /**
         * 初始化其他插件
         */
        /* mysql 数据库插件 */
        new MysqlDataSourcePlugin().start();
    }
```

### 3. RouterPlugin
主要针对我自己框架所开发的插件，主要支持如下
```
1. 扫描带有 @Controller 注解的类，进行添加至路由下，相关的类需要写在 *.controller.* 目录下
```

使用方式（适用于web，非web可以尝试，我未进行测试）
```
    /**
     * 配置路由
     * @param routes
     */
    public void configRoute(Routes routes) {
        /**
         * 加载路由
         */
        new RouterPlugin(routes).start();
    }
```

### 4. Sqlite3DataSourcePlugin
主要针对我自己框架所开发的插件，主要支持如下
```
1. 多数据源支持，配置文件采用 ini 的格式进行编写
2. 支持 JFinal Model 的@Model 注解扫描映射
3. 支持 SQL 模板化（类似 MyBatis的Mapper文件）,扫描指定路径下的 *.stl 文件，文件里面格式是用 enjoy template来解析
```

使用方式（适用于web，非web可以尝试，我未进行测试）
```
public void configPlugin(Plugins plugins) {
    plugins.add(new Sqlite3DataSourcePlugin());
}
```


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

