package com.jfinal.annotation;


import java.lang.annotation.*;

/**
 * 实体类注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Model {

    String ds() default "";

    String table();

    String key() default "";

    String dbType() default "mysql";
}
