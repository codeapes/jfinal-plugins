package com.jfinal.kit;

/**
 * 为了兼容主流的json接口规范，主要定义的key为
 * code 错误编码 0 为成功 非零为各种错误
 * message 提示的消息
 * data 存储的数据
 */
public class Result extends Ret {

    private static final String MESSAGE = "message";

    private static final String CODE = "code";

    private static final String DATA = "data";


    public Result set(Object key, Object value){
        super.set(key,value);
        return this;
    }

    public static Result _new(){
        return new Result();
    }

    public static Result _new(Object key, Object value){
        return _new().set(key,value);
    }


    public Result setOk() {
        return setOk(null);
    }

    public Result setOk(Object message){
        this.set(CODE,0);
        if(message != null){
            this.set(MESSAGE,message);
        }
        return this;
    }

    public Result setSuccess(){
        return setOk();
    }

    public Result setSuccess(Object message){
        return setOk(message);
    }


    public Result setFail(int code,Object message) {
        this.set(CODE,code);
        if(message != null){
            this.set(MESSAGE,message);
        }
        return this;
    }

    public boolean isOk() {
        if(this.getInt(CODE)!=null){
            if( this.getInt(CODE) == 0){
                return true;
            }
        }
        return false;
    }

    public boolean isFail() {
        return !isOk();
    }

    /*public Result setState(String stateKey,boolean state,String messageKey,Object message){
        this.set(stateKey == null ? "success" : stateKey,state);
        this.set(messageKey == null ? "success" : messageKey,message);
        return this;
    }

    public Result setState(boolean state,String messageKey,Object message){
        return setState(null,state,messageKey,message);
    }

    public Result setState(boolean state,Object message){
        return setState(null,state,null,message);
    }

    public Result setOk(Object message){
        return setOk(MESSAGE,message);
    }

    public Result setOk(String messageKey,Object message){
        this.setOk();
        this.set(messageKey,message);
        return this;
    }

    public Result setFail(Object message){
        return setFail(MESSAGE,message);
    }

    public Result setFail(String messageKey,Object message){
        this.setFail();
        this.set(messageKey,message);
        return this;
    }

    public Result setState(String state,Object message){
        this.set("state",state);
        this.set(MESSAGE,message);
        return this;
    }

    public Result setState(String state,String messageKey,Object message){
        this.set("state",state);
        this.set(messageKey,message);
        return this;
    }*/

    public Result setData(Object data){
        return set("data",data);
    }
}
