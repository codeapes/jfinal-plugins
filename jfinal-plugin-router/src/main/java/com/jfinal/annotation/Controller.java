package com.jfinal.annotation;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * Jfinal Controller 扫描注解类
 * @author LinCH
 * @version 1.0
 * Date 2017/06/23
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Controller {

	/**
	 * 访问的URI
	 */
	String uri();
	/**
	 * 视图路径
	 */
	String viewPath() default "";

}
